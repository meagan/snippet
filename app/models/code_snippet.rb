class CodeSnippet < ActiveRecord::Base
  validates :title, presence: true
  validates :language, presence: true
  validates :body, presence: true
end
