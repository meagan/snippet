class SnippetsController < ApplicationController
  def index
    @snippets = CodeSnippet.all
  end

  def show
    @snippet = CodeSnippet.find_by_id(params[:id])
  end

  def new
    @snippet = CodeSnippet.new
  end

  def create
    @snippet = CodeSnippet.new(snippet_params)
    if @snippet.save
      redirect_to snippets_path
    else
      render 'new'
    end
  end

  def edit
    @snippet = CodeSnippet.find_by_id(params[:id])
  end

  def update
    @snippet = CodeSnippet.find_by_id(params[:id])
    if @snippet.update_attributes(snippet_params)
      redirect_to snippet_path(params[:id])
    else
      render 'edit'
    end
  end

  private
  def snippet_params
    params.require(:snippet).permit(:title, :language, :description, :body)
  end
end
