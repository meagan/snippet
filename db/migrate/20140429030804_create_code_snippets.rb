class CreateCodeSnippets < ActiveRecord::Migration
  def change
    create_table :code_snippets do |t|
      t.string :title
      t.string :language
      t.text :description
      t.text :body
      t.timestamps
    end
  end
end
