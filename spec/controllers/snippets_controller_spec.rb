require 'spec_helper'

describe SnippetsController do
  let!(:snippet) { CodeSnippet.create(:title => "Some Title",
                                     :language => "Some language",
                                     :body => "Some body")}
  describe "GET #index" do
    it "populates an array of snippets" do
      get :index
      expect(assigns[:snippets]).to eq([snippet])
    end

    it "renders the :index view" do
      get :index
      expect(response).to render_template("index")
    end
  end

  describe "GET #show" do
    it "assigns the requested snippet to @snippet" do
      get :show, {:id => snippet.id}
      expect(assigns[:snippet]).to eq(snippet)
    end

    it "renders the :show template" do
      get :show, {:id => snippet.id}
      expect(response).to render_template("show")
    end
  end

  describe "GET #new" do
    it "assigns a new CodeSnippet to @snippet" do
      get :new
      expect(assigns[:snippet]).to be_instance_of(CodeSnippet)
    end

    it "renders the :new template" do
      get :new
      expect(response).to render_template("new")
    end
  end

  describe "GET #edit" do
    it "assigns an existing CodeSnippet to @snippet" do
      get :edit, :id => snippet.id
      expect(assigns[:snippet]).to eq(snippet)
    end

    it "renders the edit template" do
      get :edit, :id => snippet.id
      expect(response).to render_template("edit")
    end
  end

  describe "POST #create" do
    context "with valid attributes" do
      it "saves the new snippet" do
        expect { post :create, snippet: {:title => "Some Title",
          :language => "Some Language",
          :body => "Some body"}}.to change(CodeSnippet, :count).by(1)
      end

      it "redirects to home page" do
        post :create, snippet: {:title => "Some Title",
          :language => "Some Language",
          :body => "Some body"}
        expect(response).to redirect_to(snippets_path)
      end
    end

    context "with invalid attributes" do
      it "doesnt save the snippet" do
        expect { post :create, snippet: {:title => ""}}.to change(CodeSnippet, :count).by(0)
      end

      it "renders the new template" do
        post :create, snippet: {:title => ""}
        expect(response).to render_template("new")
      end
    end
  end

  describe "PUT #update" do
    context "with valid attrs" do
      it "updates the snippet" do
        put :update, :id => snippet.id, snippet: {:title => "The New Title"}
        CodeSnippet.find_by_id(snippet.id).title.should == "The New Title"
      end

      it "redirects to snippet show" do
        put :update, :id => snippet.id, snippet: {:title => "Some New Title"}
        expect(response).to redirect_to(snippet_path(snippet.id))
      end
    end

    context "with invalid attrs" do
      it "renders edit template" do
        put :update, :id => snippet.id, snippet: {:title => ""}
        expect(response).to render_template("edit")
      end
    end
  end
end
